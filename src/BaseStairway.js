
import { StairwayData } from './StairwayData.js'

/**
 * The Stairway embedded document model.
 * @extends Document
 * @memberof documents
 *
 * @param {object} data                     Initial data from which to construct the embedded document.
 * @property {data.StairwayData} data       The constructed data object for the embedded document.
 */
export class BaseStairway extends foundry.abstract.Document {
  /** @inheritdoc */
  static get schema () {
    return StairwayData
  }

  /** @inheritdoc */
  static get metadata () {
    return foundry.utils.mergeObject(super.metadata, {
      name: 'Stairway',
      collection: 'stairways',
      label: 'DOCUMENT.Stairway',
      isEmbedded: true
    })
  }
}
